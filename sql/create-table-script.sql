use itdict_db;
create table roles(
id int auto_increment,
`name` nvarchar(50),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (id)
);
create table jobs(
id int auto_increment,
`name` nvarchar(50),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (id)
);

create table users(
username varchar(50),
email varchar(255),
phone varchar(13),
fullname nvarchar(255),
dob date,
`password` varchar(50),
`active` bit,
role_id int,
job_id int,
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (username)
);

create table categories(
id int auto_increment,
`name` nvarchar(50),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (id)
);

create table words(
id int auto_increment,
keyword nvarchar(100),
defination longtext,
syntax longtext,
publish bit,
times int,
`active` bit,
sub_title nvarchar(100),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (id)
);

create table words_map_categories(
word_id int,
category_id int,
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50)
);
