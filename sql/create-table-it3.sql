create table permissions(
id int auto_increment primary key,
`key` nvarchar(100),
`name` nvarchar(100),
`user` bit,
approver bit,
administrator bit,
system_admin bit,
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50)
);

create table user_reports
(
id int auto_increment primary key,
username varchar(50),
reason nvarchar(500),
`status` bit,
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
foreign key (username) references users(username),
foreign key (created_by) references users(username),
foreign key (updated_by) references users(username)
);

create table dicts_map_words(
id int auto_increment primary key,
dict_id int,
word_id int,
`definition` longtext,
keyword varchar(100),
created datetime,
created_by varchar(50),
updated datetime,
updated_by varchar(50),
primary key (id),
foreign key (dict_id) references dictionaries(id),
foreign key (word_id) references words(id)
);
