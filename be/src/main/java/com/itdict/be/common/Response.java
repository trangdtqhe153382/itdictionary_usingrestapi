package com.itdict.be.common;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Response<T> {
    @JsonProperty("status")
    private int status;
    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private T data;

    @JsonProperty("list")
    private List<T> list;

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}
