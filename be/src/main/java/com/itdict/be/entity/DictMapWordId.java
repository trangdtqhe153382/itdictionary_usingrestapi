package com.itdict.be.entity;

import jakarta.persistence.Embeddable;

import java.io.Serializable;
@Embeddable
public class DictMapWordId implements Serializable {
    private Integer dictId;
    private Integer wordId;

    public DictMapWordId() {
    }

    public DictMapWordId(Integer dictId, Integer wordId) {
        dictId = dictId;
        wordId = wordId;
    }

    public Integer getDictId() {
        return this.dictId;
    }

    public void setDictId(Integer dictId) {
        this.dictId = dictId;
    }

    public Integer getWordId() {
        return this.wordId;
    }

    public void setWordId(Integer wordId) {
        this.wordId = wordId;
    }
}
