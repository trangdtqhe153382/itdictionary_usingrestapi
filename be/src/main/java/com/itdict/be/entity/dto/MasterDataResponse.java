package com.itdict.be.entity.dto;

import com.itdict.be.entity.Permissions;

import java.util.List;

public class MasterDataResponse {
    private List<String> categories;
    private List<String> jobs;
    private List<Permissions> permissions;

    public MasterDataResponse() {
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getJobs() {
        return jobs;
    }

    public void setJobs(List<String> jobs) {
        this.jobs = jobs;
    }

    public List<Permissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permissions> permissions) {
        this.permissions = permissions;
    }
}
