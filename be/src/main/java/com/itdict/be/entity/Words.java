package com.itdict.be.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Date;

@Entity
public class Words {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String keyword;
    private String defination;
    private String syntax;
    private Boolean publish;
    private Integer times;
    private Boolean active;
    private String subTitle;
    private Date created;
    private String createdBy;
    private Date updated;
    private String updatedBy;

    public Words() {
    }

    public Words(Integer id, String keyword, String defination, String syntax, Boolean publish, Integer times, Boolean active, String sub_title, Date created, String created_by, Date updated, String updated_by) {
        this.id = id;
        this.keyword = keyword;
        this.defination = defination;
        this.syntax = syntax;
        this.publish = publish;
        this.times = times;
        this.active = active;
        this.subTitle = sub_title;
        this.created = created;
        this.createdBy = created_by;
        this.updated = updated;
        this.updatedBy = updated_by;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDefination() {
        return defination;
    }

    public void setDefination(String defination) {
        this.defination = defination;
    }

    public String getSyntax() {
        return syntax;
    }

    public void setSyntax(String syntax) {
        this.syntax = syntax;
    }

    public Boolean getPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSub_title(String sub_title) {
        this.subTitle = sub_title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String created_by) {
        this.createdBy = created_by;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updated_by) {
        this.updatedBy = updated_by;
    }
}
