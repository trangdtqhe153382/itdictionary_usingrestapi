package com.itdict.be.entity;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class WordMapCateId implements Serializable {
    private Integer wordId;
    private Integer categoryId;

    public WordMapCateId() {
    }

    public WordMapCateId(Integer wordId, Integer categoryId) {
        this.wordId = wordId;
        this.categoryId = categoryId;
    }

    public Integer getWordId() {
        return wordId;
    }

    public void setWordId(Integer wordId) {
        this.wordId = wordId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
