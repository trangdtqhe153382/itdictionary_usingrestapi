package com.itdict.be.entity;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

@Entity
public class WordsMapCategories {
    @EmbeddedId
    private WordMapCateId id;

    public WordsMapCategories(WordMapCateId id) {
        this.id = id;
    }

    public WordsMapCategories() {
    }

    public WordMapCateId getId() {
        return id;
    }

    public void setId(WordMapCateId id) {
        this.id = id;
    }
}
