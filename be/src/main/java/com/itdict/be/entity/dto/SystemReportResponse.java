package com.itdict.be.entity.dto;

public class SystemReportResponse {
    private Integer totalUser;
    private Integer newUser;
    private Integer inactiveUser;
    private Integer totalWord;
    private Integer newWord;
    private Integer like;
    private Integer dislike;
    private Integer totalContribution;
    private Integer contributionInMonth;
    private Double approvedRate;
    private Double rejectedRate;

    public Integer getTotalUser() {
        return totalUser;
    }

    public void setTotalUser(Integer totalUser) {
        this.totalUser = totalUser;
    }

    public Integer getNewUser() {
        return newUser;
    }

    public void setNewUser(Integer newUser) {
        this.newUser = newUser;
    }

    public Integer getInactiveUser() {
        return inactiveUser;
    }

    public void setInactiveUser(Integer inactiveUser) {
        this.inactiveUser = inactiveUser;
    }

    public Integer getTotalWord() {
        return totalWord;
    }

    public void setTotalWord(Integer totalWord) {
        this.totalWord = totalWord;
    }

    public Integer getNewWord() {
        return newWord;
    }

    public void setNewWord(Integer newWord) {
        this.newWord = newWord;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public Integer getDislike() {
        return dislike;
    }

    public void setDislike(Integer dislike) {
        this.dislike = dislike;
    }

    public Integer getTotalContribution() {
        return totalContribution;
    }

    public void setTotalContribution(Integer totalContribution) {
        this.totalContribution = totalContribution;
    }

    public Integer getContributionInMonth() {
        return contributionInMonth;
    }

    public void setContributionInMonth(Integer contributionInMonth) {
        this.contributionInMonth = contributionInMonth;
    }

    public Double getApprovedRate() {
        return approvedRate;
    }

    public void setApprovedRate(Double approvedRate) {
        this.approvedRate = approvedRate;
    }

    public Double getRejectedRate() {
        return rejectedRate;
    }

    public void setRejectedRate(Double rejectedRate) {
        this.rejectedRate = rejectedRate;
    }
}
