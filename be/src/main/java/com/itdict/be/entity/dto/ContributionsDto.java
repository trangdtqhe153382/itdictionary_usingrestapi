package com.itdict.be.entity.dto;

import com.itdict.be.entity.Words;

import java.util.Date;
import java.util.List;

public class ContributionsDto {
    private String  keyword;
    private int id;
    private Integer wordId;

    private String newDefinition;

    private String newSyntax;

    private String newSubTitle;

    private String newCategories;

    private String comment;

    private Boolean status;

    private String notes;

    private Date created;

    private String createdBy;

    private Date updated;

    private String updatedBy;

    private List<Integer> categoryList;

    private String Contact;
    private WordDto orgWord;
    private WordDto newWord;

    private String approverContact;

    public ContributionsDto() {
    }

    public String getApproverContact() {
        return approverContact;
    }

    public void setApproverContact(String approverContact) {
        this.approverContact = approverContact;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getWordId() {
        return wordId;
    }

    public void setWordId(Integer wordId) {
        this.wordId = wordId;
    }

    public String getNewDefinition() {
        return newDefinition;
    }

    public void setNewDefinition(String newDefinition) {
        this.newDefinition = newDefinition;
    }

    public String getNewSyntax() {
        return newSyntax;
    }

    public void setNewSyntax(String newSyntax) {
        this.newSyntax = newSyntax;
    }

    public String getNewSubTitle() {
        return newSubTitle;
    }

    public void setNewSubTitle(String newSubTitle) {
        this.newSubTitle = newSubTitle;
    }

    public String getNewCategories() {
        return newCategories;
    }

    public void setNewCategories(String newCategories) {
        this.newCategories = newCategories;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<Integer> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Integer> categoryList) {
        this.categoryList = categoryList;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public WordDto getOrgWord() {
        return orgWord;
    }

    public void setOrgWord(WordDto orgWord) {
        this.orgWord = orgWord;
    }

    public WordDto getNewWord() {
        return newWord;
    }

    public void setNewWord(WordDto newWord) {
        this.newWord = newWord;
    }
}
