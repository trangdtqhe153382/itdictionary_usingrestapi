package com.itdict.be.entity.dto;

import com.itdict.be.entity.Permissions;

import java.util.ArrayList;
import java.util.List;

public class MenuDto {
    private String key, label;
    private List<PermissionDto> screenPermission;

    public MenuDto() {
    }

    public MenuDto(String key, String label, List<Permissions> screenPermission) {
        this.key = key;
        this.label = label;
        List<PermissionDto> permissions = new ArrayList<>();
        for(Permissions p : screenPermission){
            permissions.add(new PermissionDto(p.getKey(),p.getName(),p.getValue()));
        }
        this.screenPermission = permissions;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<PermissionDto> getScreenPermission() {
        return screenPermission;
    }

    public void setScreenPermission(List<PermissionDto> screenPermission) {
        this.screenPermission = screenPermission;
    }
}
