package com.itdict.be.repository;

import com.itdict.be.entity.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionsRepository extends JpaRepository<Permissions,Integer> {
    @Query("SELECT p from Permissions p where p.key = :key and p.user = true")
    List<Permissions> getPermissionsForUser(@Param("key") String key);
    @Query("SELECT p from Permissions p where p.key = :key and p.approver = true")
    List<Permissions> getPermissionsForApprover(@Param("key") String key);
    @Query("SELECT p from Permissions p where p.key = :key and p.administrator = true")
    List<Permissions> getPermissionsForAdmin(@Param("key") String key);
    @Query("SELECT p from Permissions p where p.key = :key and p.systemAdmin = true")
    List<Permissions> getPermissionsForSystemAdmin(@Param("key") String key);
    @Query("select distinct p.value from Permissions p where p.value is not null and p.key = 'menu'")
    List<String> getPermissionKey();
    @Query("SELECT p from Permissions p where p.key = :key and p.name = :name")
    Permissions getPermissionsBy(@Param("key") String key, @Param("name") String name);

}
