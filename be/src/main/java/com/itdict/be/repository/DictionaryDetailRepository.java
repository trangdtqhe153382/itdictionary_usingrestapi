package com.itdict.be.repository;

import com.itdict.be.entity.DictsMapWords;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictionaryDetailRepository extends JpaRepository<DictsMapWords, Integer> {
    @Query("select d from DictsMapWords d where d.dictionaries.id = :id")
    List<DictsMapWords> getList(@Param("id") Integer dictionaryId);
}
