package com.itdict.be.repository;

import com.itdict.be.entity.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {
    @Query("select r from Roles r where r.name = :role")
    public Roles getByName(@Param("role") String role);
}
