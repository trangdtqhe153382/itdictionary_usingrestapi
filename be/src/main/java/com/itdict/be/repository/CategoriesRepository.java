package com.itdict.be.repository;

import com.itdict.be.entity.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository extends JpaRepository<Categories, Integer> {
    @Query("select c.name from Categories c")
    public List<String> getAllCategories();

    @Query("select c from Categories c where c.name = :name")
    public Categories getByName(@Param("name") String name);
}
