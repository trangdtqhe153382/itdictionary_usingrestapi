package com.itdict.be.repository;

import com.itdict.be.entity.DictMapCateId;

import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.DictsMapCates;
import org.springframework.data.jpa.repository.JpaRepository;

import com.itdict.be.entity.DictsMapCates;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictMapCateRepository extends JpaRepository<DictsMapCates, DictMapCateId> {

    @Query("SELECT dic FROM DictsMapCates dic WHERE dic.id.dictId = :dictId")
    List<DictsMapCates> getListCateByDict(@Param("dictId") Integer dictId);

    @Query("SELECT dic FROM DictsMapCates dic WHERE dic.id.dictId = :dictionaryId")
    DictsMapCates getDictMapCates(@Param("dictionaryId") Integer dictId);

    @Modifying
    @Transactional
    @Query(
            value =
                    "insert into DictsMapCates(dict_id, cate_id) values (:dict_id, :cate_id)",
            nativeQuery = true)
    void save(@Param("dict_id") Integer dict_id, @Param("cate_id") Integer cate_id);

}
