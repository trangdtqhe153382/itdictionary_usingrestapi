package com.itdict.be.repository;

import com.itdict.be.entity.Jobs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Jobs, Integer> {
    @Query("select j.name from Jobs j")
    public List<String> getAllJobs();

    @Query("select j from Jobs j where j.name = :name")
    public Jobs getByName(@Param("name") String name);
}
