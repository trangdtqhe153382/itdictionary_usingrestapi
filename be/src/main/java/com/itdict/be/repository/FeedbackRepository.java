package com.itdict.be.repository;

import com.itdict.be.entity.Dictionaries;
import com.itdict.be.entity.Feedbacks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedbacks,Integer> {
    @Query("SELECT feedbacks FROM Feedbacks feedbacks")
    List<Feedbacks> getAllFeedbacks();

    @Query("SELECT f from Feedbacks f where f.rate = :rate order by f.created")
    List<Feedbacks> getReport(@Param("rate") Integer rate);

    @Query("SELECT feedbacks FROM Feedbacks feedbacks where feedbacks.rate in :rate")
    List<Feedbacks> getSystemReport(@Param("rate") List<Integer> rate);
}
