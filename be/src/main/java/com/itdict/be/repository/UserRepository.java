package com.itdict.be.repository;

import com.itdict.be.entity.Users;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Date;

@Repository
public interface UserRepository extends JpaRepository<Users, String> {
    @Query("SELECT u FROM Users u WHERE u.username = :username and u.password = :password and u.active = true")
    Users login(@Param("username") String username, @Param("password") String password);

    @Query("SELECT u FROM Users u WHERE u.username = :username")
    List<Users> search(Pageable pageable, @Param("username") String username);

    @Query("SELECT u FROM Users u")
    List<Users> searchAll(Pageable pageable);

    @Query("SELECT u FROM Users u WHERE u.username = :username")
    List<Users> searchNoPaging(@Param("username") String username);

    @Query("SELECT u FROM Users u WHERE u.username = :username")
    Users getUsersByUsername(@Param("username") String username);

    @Query("select u.username from Users u inner join Roles r on u.roleId = r.id " +
            "where r.name = :role and u.active = true")
    List<String> getUsersByRole(@Param("role") String role);

    @Query("select u.username from Users u where u.active=false")
    List<String> getInactiveUsers();

    @Query("select u.username from Users u inner join Roles r on u.roleId = r.id " +
            "where u.active=true and r.name <> :role and r.name <> 'System.Admin' " +
            "and u.username like :searchValue")
    List<String> searchUsers(@Param("role") String role,
                                      @Param("searchValue") String searchValue, Pageable pageable);

    @Query("select u.username from Users u inner join Roles r on u.roleId = r.id " +
            "where u.active = false and datediff(:curDate,u.updated) <= 7")
    List<Users> getInactiveReport(@Param("curDate")Date currentDate);

    @Query("select u.username from Users u inner join Roles r on u.roleId = r.id " +
            "where u.active = true and datediff(:curDate,u.created) <= 7")
    List<Users> getNewReport(@Param("curDate")Date currentDate);
}
