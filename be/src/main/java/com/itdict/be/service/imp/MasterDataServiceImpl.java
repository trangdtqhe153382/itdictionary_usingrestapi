package com.itdict.be.service.imp;

import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.dto.MasterDataResponse;
import com.itdict.be.repository.CategoriesRepository;
import com.itdict.be.repository.JobRepository;
import com.itdict.be.repository.PermissionsRepository;
import com.itdict.be.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MasterDataServiceImpl implements MasterDataService {
    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private PermissionsRepository permissionsRepository;
    @Override
    public MasterDataResponse init() {
        MasterDataResponse response = new MasterDataResponse();
        response.setCategories(categoriesRepository.getAllCategories());
        response.setJobs(jobRepository.getAllJobs());
        response.setPermissions(permissionsRepository.findAll());
        return response;
    }

    @Override
    public List<String> getPermissionKeys() {
        List<String> res = permissionsRepository.getPermissionKey();
        res.add("menu");
        return res;
    }

}
