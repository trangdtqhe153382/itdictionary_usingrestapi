package com.itdict.be.service.imp;

import com.itdict.be.entity.Jobs;
import com.itdict.be.repository.JobRepository;
import com.itdict.be.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobServiceImpl implements JobService {
    @Autowired
    private JobRepository jobRepository;
    @Override
    public List<Jobs> getAll() {
        return jobRepository.findAll();
    }

    @Override
    public Jobs getByName(String name) {
        return jobRepository.getByName(name);
    }

    @Override
    public void delete(Integer id) {
        jobRepository.deleteById(id);
    }

    @Override
    public void save(Jobs jobs) {
        jobRepository.save(jobs);
    }
}
