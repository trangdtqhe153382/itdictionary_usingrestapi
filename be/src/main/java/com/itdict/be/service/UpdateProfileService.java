package com.itdict.be.service;

import com.itdict.be.entity.Users;

public interface UpdateProfileService {
    public Users getUser(String username);
    public void save(Users user);
}
