package com.itdict.be.service;

import com.itdict.be.entity.Jobs;

import java.util.List;

public interface JobService {
    public List<Jobs> getAll();
    public Jobs getByName(String name);
    public void delete(Integer id);
    public void save(Jobs jobs);
}
