package com.itdict.be.service.imp;

import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.Roles;
import com.itdict.be.entity.dto.MenuDto;
import com.itdict.be.repository.PermissionsRepository;
import com.itdict.be.repository.RoleRepository;
import com.itdict.be.service.PermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PermissionsServiceImpl implements PermissionsService {
    @Autowired
    private PermissionsRepository permissionsRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Override
    public List<MenuDto> getMenu(Integer roleId) {
        List<MenuDto> res = new ArrayList<>();
        Roles role = roleRepository.getReferenceById(roleId);
        if(role != null){
            List<Permissions> menus;
            switch (role.getName()){
                case "User":
                    menus = permissionsRepository.getPermissionsForUser("menu");
                    if(menus != null){
                        for (Permissions item : menus){
                            List<Permissions> screens = permissionsRepository.getPermissionsForUser(item.getValue());
                            res.add(new MenuDto(item.getValue(),item.getName(),screens));
                        }
                    }
                    break;
                case "Approver":
                    menus = permissionsRepository.getPermissionsForApprover("menu");
                    if(menus != null){
                        for (Permissions item : menus){
                            List<Permissions> screens = permissionsRepository.getPermissionsForApprover(item.getValue());
                            res.add(new MenuDto(item.getValue(),item.getName(),screens));
                        }
                    }
                    break;
                case "Administrator":
                    menus = permissionsRepository.getPermissionsForAdmin("menu");
                    if(menus != null){
                        for (Permissions item : menus){
                            List<Permissions> screens = permissionsRepository.getPermissionsForAdmin(item.getValue());
                            res.add(new MenuDto(item.getValue(),item.getName(),screens));
                        }
                    }
                    break;
                case "System.Admin":
                    menus = permissionsRepository.getPermissionsForSystemAdmin("menu");
                    if(menus != null){
                        for (Permissions item : menus){
                            List<Permissions> screens = permissionsRepository.getPermissionsForSystemAdmin(item.getValue());
                            res.add(new MenuDto(item.getValue(),item.getName(),screens));
                        }
                    }
                    break;
                default:
                    res = null;
                    break;
            }
        }
        return res;
    }

    @Override
    public void save(Permissions permission) {
        permissionsRepository.save(permission);
    }

    @Override
    public boolean checkExist(Permissions permission) {
        Permissions p = permissionsRepository.getPermissionsBy(permission.getKey(), permission.getName());
        return p != null;
    }

    @Override
    public void delete(Integer id) {
        permissionsRepository.deleteById(id);
    }

}
