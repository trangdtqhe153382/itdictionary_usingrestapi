package com.itdict.be.service.imp;

import com.itdict.be.entity.Roles;
import com.itdict.be.repository.RoleRepository;
import com.itdict.be.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Roles> getAll() {
        return roleRepository.findAll();
    }

    @Override
    public Roles getByName(String name) {
        return roleRepository.getByName(name);
    }
}
