package com.itdict.be.service;

import com.itdict.be.entity.Roles;

import java.util.List;

public interface RoleService {
    public List<Roles> getAll();
    public Roles getByName(String name);
}
