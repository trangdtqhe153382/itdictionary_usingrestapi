package com.itdict.be.service.imp;

import com.itdict.be.entity.*;
import com.itdict.be.entity.dto.DictionariesDto;
import com.itdict.be.entity.dto.WordDto;
import com.itdict.be.repository.*;
import com.itdict.be.service.DictionariesService;
import com.itdict.be.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class DictionariesServiceImpl implements DictionariesService {
    @Autowired
    private DictionariesRepository dictionariesRepository;
    @Autowired
    private DictMapCateRepository dictMapCateRepository;
    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private DictionaryDetailRepository dictionaryDetailRepository;
    @Override
    public Dictionaries getByName(String name) {
        return dictionariesRepository.getDictionariesByName(name);
    }

    @Override
    public void saveMapCate(DictsMapCates map) {
        dictMapCateRepository.save(map);
    }

    @Override
    public List<DictionariesDto>  getAllByUsername(String username) {
        List<Dictionaries> dictionaries = dictionariesRepository.getAllByUsername(username);
        return getDictionariesDtos(dictionaries);
    }

    private List<DictionariesDto> getDictionariesDtos(List<Dictionaries> dictionaries) {
        List<DictionariesDto> returnRs = new ArrayList<>();
        for (Dictionaries d : dictionaries){
            List<DictsMapCates> dmc = dictMapCateRepository.getListCateByDict(d.getId());
            List<String> cateIds = new ArrayList<>();
            for(DictsMapCates dm: dmc){
                Categories c = categoriesRepository.getReferenceById(dm.getId().getCateId());
                cateIds.add(c.getName());
            }

            if(d.getCreated() != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d.getCreated());
                calendar.add(Calendar.HOUR_OF_DAY, 7);
                d.setCreated(calendar.getTime());
            }
            DictionariesDto dto = new DictionariesDto(
                    d.getId(),d.getName(), cateIds, d.getViewBy(), d.getCreatedBy(), d.getCreated()
            );
            List<DictsMapWords> list = dictionaryDetailRepository.getList(d.getId());
            dto.setNumOfWords(list.isEmpty() ? 0 : list.size());
            dto.setEditBy(d.getEditBy());
            returnRs.add(dto);
        }
        return returnRs;
    }

    @Override
    public List<DictionariesDto> search(String name) {
        List<Dictionaries> dictionaries = dictionariesRepository.search('%'+name+'%');
        return getDictionariesDtos(dictionaries);
    }

    @Override
    public Dictionaries getById(Integer id) {
        return dictionariesRepository.getAllById(id);
    }

    @Override
    public Dictionaries update(DictionariesDto dictionaries) {
        Dictionaries d = new Dictionaries(dictionaries, dictionaries.getId());
        dictionariesRepository.save(d);

        List<DictsMapCates> map = dictMapCateRepository.getListCateByDict(dictionaries.getId());
        for(DictsMapCates mc : map){
            dictMapCateRepository.delete(mc);
        }

        if(dictionaries.getCategories() != null){
            for(String cate : dictionaries.getCategories()){
                DictsMapCates m = new DictsMapCates();
                Categories c = categoriesRepository.getByName(cate);
                m.setId(new DictMapCateId(d.getId(), c.getId()));
                dictMapCateRepository.save(m);
            }
        }
        return d;
    }

    @Override
    public Dictionaries save(Dictionaries dictionaries) {
        dictionaries.setCreated(new Date(System.currentTimeMillis()));
        return dictionariesRepository.save(dictionaries);
    }

}
