package com.itdict.be.service;

import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.entity.dto.UserDto;
//import com.itdict.be.restAPI.ForgotPassWordRestAPI;
import com.itdict.be.entity.dto.UserPermissionDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Date;
import java.util.Map;

//@Service
public interface UserService {
    public List<Users> getAll();
    public UserDto getLoginInfo(String username, String password);
    public List<UserDto> search(Integer currentPage, String username);
    public Integer countTotal(String username);
    public void delete(String username);
    public void save(Users u);
    public Users getByUsername(String username);
    public Users forgotPassword(Users u);
    public UserDto resetPassword(Users u);
    public UserPermissionDto getUserPermission();
    public List<String> search(String us, String role);
    public SystemReportResponse getUserReport();
}
