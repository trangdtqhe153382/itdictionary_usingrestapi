package com.itdict.be.service;

import com.itdict.be.entity.Permissions;
import com.itdict.be.entity.dto.MenuDto;

import java.util.List;

public interface PermissionsService {
    public List<MenuDto> getMenu(Integer roleId);
    public void save(Permissions permission);
    public boolean checkExist(Permissions permission);
    public void delete(Integer id);
}
