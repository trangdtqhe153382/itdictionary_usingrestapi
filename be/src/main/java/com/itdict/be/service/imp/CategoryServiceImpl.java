package com.itdict.be.service.imp;

import com.itdict.be.entity.Categories;
import com.itdict.be.repository.CategoriesRepository;
import com.itdict.be.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoriesRepository categoriesRepository;
    @Override
    public List<Categories> getAll() {
        return categoriesRepository.findAll();
    }

    @Override
    public void create(Categories category) {
        categoriesRepository.save(category);
    }

    @Override
    public void delete(Integer id) {
        categoriesRepository.deleteById(id);
    }

    @Override
    public Categories getByName(String name) {
        return categoriesRepository.getByName(name);
    }
}
