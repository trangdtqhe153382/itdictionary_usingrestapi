package com.itdict.be.service;

import com.itdict.be.entity.Contributions;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.ContributionCountDto;
import com.itdict.be.entity.dto.ContributionsDto;
import com.itdict.be.entity.dto.SystemReportResponse;
import com.itdict.be.entity.dto.UserDto;

import java.util.List;

public interface ContributionService {
    public List<Contributions> getAll();

    public List<ContributionsDto> search(Integer currentPage, String keyword, String username);

    public Integer countTotal(String keyWord, String username);

    public ContributionsDto approve(ContributionsDto dto);
    public Contributions findById(Integer id);
    public void save(Contributions c);
    public List<ContributionCountDto> countTop();
    public void delete(Integer id);
    public List<ContributionsDto> getList();
    public Contributions addContribution(Contributions c);
    public ContributionsDto getById(Integer id);
    public SystemReportResponse getReport(SystemReportResponse res);

}
