package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Contributions;
import com.itdict.be.entity.Words;
import com.itdict.be.entity.dto.ContributionsDto;
import com.itdict.be.service.ContributionService;
import com.itdict.be.service.UserService;
import com.itdict.be.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/admin/contribution")
public class ContributionManagementRestAPI {
    @Autowired
    private WordService wordService;
    @Autowired
    private ContributionService contributionService;
    @Autowired
    private UserService userService;

    @GetMapping("/search")
    @CrossOrigin
    public ResponseEntity<Response<ContributionsDto>> search(@RequestParam Integer currentPage,
                                                             @RequestParam String keyWord,
                                                             @RequestParam String username) {
        Response<ContributionsDto> response = new Response<>();
        try {
            List<ContributionsDto> contributions = contributionService.search(currentPage, keyWord, username);
            response.setStatus(200);
            response.setList((contributions));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countTotal")
    @CrossOrigin
    public ResponseEntity<Response<Integer>> countTotal(@RequestParam String keyWord, @RequestParam String username) {
        Response<Integer> response = new Response<>();
        try {
            response.setStatus(200);
            response.setData(contributionService.countTotal(keyWord, username));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/approve")
    @CrossOrigin
    public ResponseEntity<Response<Contributions>> approved(@RequestBody ContributionsDto dto) {
        Response<Contributions> response = new Response<>();
        try {
            Words exWord = wordService.getByKey(dto.getKeyword());
            if (exWord != null) {
                if (exWord.getId() != dto.getWordId()) {
                    response.setStatus(500);
                    response.setMessage("Keyword is existed. Can't approve this contribution.");
                } else {
                    // update
                    contributionService.approve(dto);
                    response.setStatus(200);
                    response.setMessage("Approve successfully.");
                }
            } else {
                // update
                contributionService.approve(dto);
                response.setStatus(200);
                response.setMessage("Approve successfully.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/reject")
    @CrossOrigin
    public ResponseEntity<Response<Contributions>> reject(@RequestParam Integer id, @RequestParam String updatedBy) {
        Response<Contributions> response = new Response<>();
        try {
            Contributions c = contributionService.findById(id);
            if (c != null) {
                c.setStatus(false);
                c.setUpdated(new Date());
                c.setUpdatedBy(userService.getByUsername(updatedBy));
                contributionService.save(c);
                response.setStatus(200);
                response.setMessage("Reject successfully!");
            } else {
                response.setStatus(201);
                response.setMessage("Contribution is not existed.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/saveNotes")
    @CrossOrigin
    public ResponseEntity<Response<Contributions>> saveNotes(@RequestBody ContributionsDto con) {
        Response<Contributions> response = new Response<>();
        try {
            Contributions c = contributionService.findById(con.getId());
            if (c != null) {
                c.setUpdated(new Date());
                c.setUpdatedBy(userService.getByUsername(con.getUpdatedBy()));
                c.setNotes(con.getNotes());
                contributionService.save(c);
                response.setStatus(200);
                response.setMessage("Save note successfully.");
            } else {
                response.setStatus(201);
                response.setMessage("Contribution is not existed.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/delete")
    @CrossOrigin
    public ResponseEntity<Response<Object>> delete(@RequestParam Integer id) {
        Response<Object> response = new Response<>();
        try {
            Contributions c = contributionService.findById(id);
            if (c != null) {
                contributionService.delete(id);
                response.setStatus(200);
                response.setMessage("Save note successfully.");
            } else {
                response.setStatus(201);
                response.setMessage("Contribution is not existed.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get-list")
    @CrossOrigin
    public ResponseEntity<Response<ContributionsDto>> getList() {
        Response<ContributionsDto> response = new Response<>();
        try {
            List<ContributionsDto> contributions = contributionService.getList();
            response.setStatus(200);
            response.setList((contributions));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
