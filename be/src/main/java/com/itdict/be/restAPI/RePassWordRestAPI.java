package com.itdict.be.restAPI;

//import org.springframework.web.bind.annotation.RequestMapping;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.RePasswordDto;
import com.itdict.be.entity.dto.UserDto;
import com.itdict.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/resetPassword")
public class RePassWordRestAPI {
    @Autowired
    private UserService userService;

    @PostMapping("/reset")
    @CrossOrigin
    public ResponseEntity<Response<UserDto>> reset(@RequestBody RePasswordDto en) {
        Response<UserDto> response = new Response<>();
        try {
            Users us = userService.getByUsername(en.getUsername());
            if (us == null) {
                response.setStatus(201);
                response.setMessage("Username is not exist.");
            }
            else {
                if(en.getCurrentPassword().isEmpty()){
                    response.setStatus(201);
                    response.setMessage("Old password is required.");
                }else{
                    if(!en.getCurrentPassword().equals(us.getPassword())){
                        response.setStatus(201);
                        response.setMessage("Old password is incorrect.");
                    }else{
                        if(en.getNewPassword().isEmpty()){
                            response.setStatus(201);
                            response.setMessage("New password is required.");
                        }else{
                            if(en.getRePassword().isEmpty()){
                                response.setStatus(201);
                                response.setMessage("Re-password is required.");
                            }else{
                                if(!en.getNewPassword().equals(en.getRePassword())){
                                    response.setStatus(201);
                                    response.setMessage("Re-password is not match new password.");
                                }else{
                                    us.setPassword(en.getNewPassword());
                                    UserDto rs = userService.resetPassword(us);
                                    if(rs == null){
                                        response.setStatus(201);
                                        response.setMessage("Reset password failed.");
                                    }else{
                                        response.setStatus(200);
                                        response.setMessage("Reset password successfully.");
                                        response.setData(rs);
                                    }
                                }
                            }
                        }
                    }
                }
            }

//            String us = userService.;
//            if (password.equals(us){
//                if (newPassword.equals("")){
//                    if (rePassword.equals(newPassword)){
//                        String name = userService.getInputUser();
//                        userService.setPassword(name,password);
//                        response.setStatus(200);
//                        response.setMessage("Update successfully.");
//                    }else{
//                        response.setStatus(201);
//                        response.setMessage("rePassword must same password.");
//                    }
//                }else {
//                    response.setStatus(201);
//                    response.setMessage("not empty.");
//                }
//            }else {
//                response.setStatus(201);
//                response.setMessage("not found.");
//            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}