package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.*;
import com.itdict.be.entity.dto.ContributionsDto;
import com.itdict.be.entity.dto.WordDto;
import com.itdict.be.service.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/word")

public class WordRestAPI {
    @Autowired
    private WordService wordService;

    @Autowired
    private ContributionService contributionService;

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    @CrossOrigin
    public ResponseEntity<Response<Contributions>> create(@RequestBody ContributionsDto cDto) {
        Response<Contributions> response = new Response<>();
        try {
            if (cDto.getKeyword().isEmpty()) {
                response.setStatus(201);
                response.setMessage("Keyword is not empty");
            } else {
                Words exW = wordService.getByKey(cDto.getKeyword());
                if (exW != null) {
                    response.setStatus(201);
                    response.setMessage("Key word is existed.");
                } else {
                    if (cDto.getNewSubTitle().isEmpty()) {
                        response.setStatus(201);
                        response.setMessage("Sub title is not empty");
                    } else {
                        if (cDto.getNewDefinition().isEmpty()) {
                            response.setStatus(201);
                            response.setMessage("Definition is not empty");
                        } else {
                            Words w = new Words();
                            w.setKeyword(cDto.getKeyword());
                            w.setActive(false);
                            w.setPublish(false);
                            w.setCreatedBy(cDto.getCreatedBy());
                            w.setCreated(new Date());
                            Words w1 = wordService.addWord(w);
                            if (w1 == null) {
                                response.setStatus(201);
                                response.setMessage("Add failed.");
                            } else {
                                Contributions c = new Contributions();
                                c.setWord(w1);
                                c.setNewDefinition(cDto.getNewDefinition());
                                c.setNewSubTitle(cDto.getNewSubTitle());
                                c.setNewSyntax(cDto.getNewSyntax());
                                c.setNewCategories(cDto.getCategoryList().stream()
                                        .map(String::valueOf)
                                        .collect(Collectors.joining(";")));
                                Users crBy = new Users();
                                crBy.setUsername(cDto.getCreatedBy());
                                c.setCreatedBy(crBy);
                                c.setCreated(new Date());
                                response = saveContribution(response, c);
                            }
                        }
                    }
                }
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update")
    @CrossOrigin
    public ResponseEntity<Response<Contributions>> update(@RequestBody ContributionsDto cDto) {
        Response<Contributions> response = new Response<>();
        try {
            Words w = wordService.getByKey(cDto.getKeyword());
            if (w == null) {
                response.setStatus(201);
                response.setMessage("Word is not existed.");
            } else {
                Contributions c = new Contributions();
                c.setWord(w);
                c.setNewDefinition(cDto.getNewDefinition());
                c.setNewSubTitle(cDto.getNewSubTitle());
                c.setNewSyntax(cDto.getNewSyntax());
                c.setNewCategories(cDto.getCategoryList().stream()
                        .map(n -> String.valueOf(n))
                        .collect(Collectors.joining(";")));
                Users crBy = new Users();
                crBy.setUsername(cDto.getCreatedBy());
                c.setCreatedBy(crBy);
                c.setCreated(new Date());
                c.setComment(cDto.getComment());
                response = saveContribution(response, c);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Response<Contributions> saveContribution(Response<Contributions> response, Contributions c) {
        Contributions ct = contributionService.addContribution(c);
        if(userService.getByUsername(ct.getCreatedBy()
                .getUsername()).getRoleId() != 2){
            ContributionsDto con = contributionService.getById(ct.getId());
            con.setUpdatedBy(ct.getCreatedBy().getUsername());
            if(con != null){
                contributionService.approve(con);
                response.setStatus(200);
                response.setMessage("Save word successfully.");
            }
            else {
                response.setStatus(201);
                response.setMessage("Add word failed.");
            }
        }
        return response;
    }

    @PostMapping("/downloadTemp")
    @CrossOrigin
    public ResponseEntity<InputStreamResource> downloadTemp() {
        try {
            File file = new File(
                    "/Users/nhatbn/se1641_swp491_g1/be/src/main/java/com/itdict/be/restAPI/Template.xlsx");
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(new FileInputStream(file)));
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(null);
        }
    }

    @PostMapping("/import")
    @CrossOrigin
    public ResponseEntity<Response<WordDto>> importFile(@RequestBody MultipartFile file) {
        Response<WordDto> response = new Response<>();
        try {
            List<WordDto> words = new ArrayList<>();
            XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
            XSSFSheet worksheet = workbook.getSheetAt(0);
            for(int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++){
                WordDto w = new WordDto();

                XSSFRow row = worksheet.getRow(i);
                w.setKeyword(row.getCell(0).getStringCellValue());
                w.setDefination(row.getCell(1).getStringCellValue());
                w.setSyntax(row.getCell(2).getStringCellValue());
                String[] categories = row.getCell(3).getStringCellValue().split("\\s*,\\s*");
                w.setCategories(Arrays.stream(categories).toList());
                w.setSubTitle(row.getCell(4).getStringCellValue());
                words.add(w);
            }
            response.setList(words);
            response.setStatus(200);
            response.setMessage("OK");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(500);
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/saveImport")
    @CrossOrigin
    public ResponseEntity<Response<Object>> saveImport(@RequestBody List<WordDto> list) {
        Response<Object> response = new Response<>();
        try {
            for(WordDto w : list){
                wordService.save(w);
            }
            response.setStatus(200);
            response.setMessage("OK");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setStatus(500);
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


//    @PostMapping("/create")
//    @CrossOrigin
//    public ResponseEntity<Response<Contributions>> addWordFromUserToPublicDictionary(@RequestParam Integer wordId) {
//        Response<Contributions> response = new Response<>();
//        try {
//            WordMapDictionary dictionaries = wordMapDictionaryService.getByWordId(wordId);
//            if (dictionaries == null){
//                response.setStatus(201);
//                response.setMessage("Word is not found.");
//            }else{
//                Contributions c = new Contributions();
//                Words w = wordService.getById(dictionaries.getWordId());
//                c.setWord(w);
//                Contributions ct = contributionsService.addContribution(c);
//                if (ct == null) {
//                    response.setStatus(201);
//                    response.setMessage("Save failed.");
//                } else {
//                    response.setStatus(200);
//                    response.setMessage("Save word successfully.");
//                    response.setData(ct);
//                }
//            }
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception ex) {
//            response.setStatus(500);
//            response.setMessage(ex.getMessage());
//            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
}
