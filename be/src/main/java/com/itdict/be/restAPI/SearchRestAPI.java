package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Words;
import com.itdict.be.entity.dto.WordDto;
import com.itdict.be.repository.WordRepository;
import com.itdict.be.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/word")
public class SearchRestAPI {

    @Autowired
    private WordService wordService;
    @Autowired
    private WordRepository wordRepository;

    @GetMapping("/search")
    @CrossOrigin
    public ResponseEntity<Response<WordDto>> searchExactly(@RequestParam String keyword) {
        Response<WordDto> response = new Response<>();
        try {
            List<WordDto> word = wordService.searchExactly("%" + keyword + "%");
            if (!word.isEmpty()) {
                Words w = wordService.getByKey(word.get(0).getKeyword());
                if (w != null) {
                    w.setTimes(w.getTimes() != null ? w.getTimes() + 1 : 1);
                    wordRepository.save(w);
                }
            }
            response.setStatus(200);
            response.setList(word);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/getRelatedResult")
    @CrossOrigin
    public ResponseEntity<Response<WordDto>> getRelatedResult(@RequestParam Integer id) {
        Response<WordDto> response = new Response<>();
        try {
//            List<WordDto> word = wordService.searchExactly("%" + keyword.toUpperCase() + "%");
            Words words = wordService.getById(id);
            if (words != null) {
                List<WordDto> reList = wordService.searchRelated(words.getSubTitle(), words.getKeyword());
                response.setStatus(200);
                response.setList(reList);
            } else {
                response.setStatus(201);
                response.setMessage("Can not find this word");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
//
//    private void searchRelated(Response<WordDto> response, String subTitle) {
//        List<WordDto> listWord = wordService.searchRelated(subTitle);
//        if (!listWord.isEmpty()) {
//            response.setStatus(200);
//            response.setList(listWord);
//        } else {
//            response.setStatus(202);
//            response.setMessage("No sub_title");
//        }
//
//    }
}
