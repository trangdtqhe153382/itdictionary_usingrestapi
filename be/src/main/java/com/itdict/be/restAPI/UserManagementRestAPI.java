package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Roles;
import com.itdict.be.entity.Users;
import com.itdict.be.entity.dto.UserPermissionDto;
import com.itdict.be.service.RoleService;
import com.itdict.be.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/admin/user")
public class UserManagementRestAPI {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @GetMapping("/search")
    @CrossOrigin
    public ResponseEntity<Response<String>> search(@RequestParam String role,
                                                    @RequestParam String username) {
        Response<String> response = new Response<>();
        try {
            List<String> users = userService.search(username, role);
            response.setStatus(200);
            response.setList((users));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getPermission")
    @CrossOrigin
    public ResponseEntity<Response<UserPermissionDto>> getPermissions() {
        Response<UserPermissionDto> response = new Response<>();
        try {
            response.setData(userService.getUserPermission());
            response.setStatus(200);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/changePermission")
    @CrossOrigin
    public ResponseEntity<Response<Object>> changePermission(@RequestParam String role,
                                                   @RequestParam String username) {
        Response<Object> response = new Response<>();
        try {
            Users u = userService.getByUsername(username);
            if(u != null) {
                switch (role) {
                    case "Active":
                        u.setActive(true);
                        u.setUpdated(new Date());
                        u.setUpdatedBy("system.admin");
                        userService.save(u);
                        response.setStatus(200);
                        response.setMessage("Update successfully!");
                        break;
                    case "Inactive":
                        u.setActive(false);
                        u.setUpdated(new Date());
                        u.setUpdatedBy("system.admin");
                        userService.save(u);
                        response.setStatus(200);
                        response.setMessage("Update successfully!");
                        break;
                    default:
                        Roles r = roleService.getByName(role);
                        if (r != null) {
                            u.setRoleId(r.getId());
                            u.setUpdated(new Date());
                            u.setUpdatedBy("system.admin");
                            userService.save(u);
                            response.setStatus(200);
                            response.setMessage("Update successfully!");
                        } else {
                            response.setStatus(500);
                            response.setMessage("Role is not found!");
                        }
                        break;
                }
            }
            else {
                response.setStatus(500);
                response.setMessage("User is not existed.");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
