package com.itdict.be.restAPI;

import com.itdict.be.common.Response;
import com.itdict.be.entity.Categories;
import com.itdict.be.entity.Jobs;
import com.itdict.be.repository.JobRepository;
import com.itdict.be.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("api/job")
public class JobRestAPI {
    @Autowired
    private JobService jobService;
    @GetMapping("/getAll")
    @CrossOrigin
    public ResponseEntity<Response<Jobs>> getAll() {
        Response<Jobs> response = new Response<>();
        try{
            response.setStatus(200);
            response.setList(jobService.getAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    @CrossOrigin
    public ResponseEntity<Response<Jobs>> create(@RequestBody Jobs jobs) {
        Response<Jobs> response = new Response<>();
        try{
            Jobs job = jobService.getByName(jobs.getName());
            if(job != null){
                response.setStatus(500);
                response.setMessage("Job is existed.");
            }else {
                jobs.setCreated(new Date());
                jobs.setCreatedBy(jobs.getCreatedBy());
                jobs.setUpdated(new Date());
                jobs.setCreatedBy(jobs.getUpdatedBy());
                jobService.save(jobs);
                response.setStatus(200);
                response.setMessage("Create successfully!");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/delete")
    @CrossOrigin
    public ResponseEntity<Response<Object>> delete(@RequestParam String name) {
        Response<Object> response = new Response<>();
        try{
            Jobs j = jobService.getByName(name);
            if(j == null){
                response.setStatus(500);
                response.setMessage("Job is not existed.");
            }
            else {
                jobService.delete(j.getId());
                response.setStatus(200);
                response.setMessage("Delete successfully!");
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception ex){
            response.setStatus(500);
            response.setMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
